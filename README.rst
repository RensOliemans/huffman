This is a short project for IT&S.

Installation
^^^^^^^^^^^^

::

    pip install .


Getting started
^^^^^^^^^^^^^^^

::

    from huffman import ex5

    ex5.main()

Example usage
^^^^^^^^^^^^^

::

    from huffman import calculator

    example_distribution = {'a': .99, 'b': .01}

    h = calculator.Huffman(example_distribution)
    h.print_tree()
    h.compare_entropy_code_length()
    h.code


Running tests
^^^^^^^^^^^^^

::

    python -m unittest discover -s tests/