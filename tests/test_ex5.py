import unittest
from ddt import ddt, data

from huffman.ex5 import find_distribution
from huffman.calculator import Huffman


@ddt
class FindDistributionTest(unittest.TestCase):
    @data(0.1, 0.1**2, 0.1**4, 0.1**10, 0.1**15)
    def test_find_distribution_small_epsilon(self, value):
        distribution = find_distribution(value)
        h = Huffman(distribution)

        self.assertGreater(h.expected_code_length, h.entropy + 1 - value,
                           f"Failed for epsilon {value}.")

    @data(0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 0.9999)
    def test_find_distribution_large_epsilon(self, value):
        distribution = find_distribution(value)
        h = Huffman(distribution)

        self.assertGreater(h.expected_code_length, h.entropy + 1 - value,
                           f"Failed for epsilon {value}.")

    @data(1.0, 1.1, 1.5, 2, 3, 5, 10, 100)
    def test_find_distribution_epsilon_above_1(self, value):
        distribution = find_distribution(value)
        h = Huffman(distribution)

        self.assertGreater(h.expected_code_length, h.entropy + 1 - value,
                           f"Failed for epsilon {value}.")


if __name__ == '__main__':
    unittest.main()
