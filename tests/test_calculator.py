import unittest
from huffman.calculator import Huffman


class HuffmanEntropyTest(unittest.TestCase):
    def test_entropy_simple_distribution(self):
        h = Huffman({'a': 0.5, 'b': 0.5})
        self.assertEqual(1, h.entropy)

    def test_entropy_large_distribution(self):
        h = Huffman({1: 0.49, 2: 0.26, 3: 0.12, 4: 0.04, 5: 0.04, 6: 0.03,
                     7: 0.02})
        self.assertEqual(2.012789664333415, h.entropy)

    def test_entropy_weird_distribution(self):
        h = Huffman({1: 1.0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0})
        self.assertEqual(0, h.entropy)


class HuffmanExpectedCodeLengthTest(unittest.TestCase):
    def test_expected_code_length_simple_distribution(self):
        h = Huffman({'a': 0.5, 'b': 0.5})
        self.assertEqual(1, h.expected_code_length)

    def test_expected_code_length_large_distribution(self):
        h = Huffman({1: 0.49, 2: 0.26, 3: 0.12, 4: 0.04, 5: 0.04, 6: 0.03,
                     7: 0.02})
        self.assertEqual(2.02, h.expected_code_length)

    def test_expected_code_length_weird_distribution(self):
        h = Huffman({1: 1.0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0})
        self.assertEqual(1, h.expected_code_length)


class HuffmanCodeTest(unittest.TestCase):
    def _compare_huffman_codes(self, expected: dict, code: dict):
        self.assertEqual(expected, {node.char: codeword
                                    for node, codeword in code.items()})

    def test_huffman_code_simple_case(self):
        h = Huffman({'a': 0.49, 'b': 0.26, 'c': 0.12, 'd': 0.04, 'e': 0.04,
                     'f': 0.03, 'g': 0.02})

        expected = {'a': '0', 'b': '11', 'c': '100', 'd': '10110',
                    'e': '10111', 'f': '10101', 'g': '10100'}
        self._compare_huffman_codes(expected, h.code)

    def test_huffman_code_above_1(self):
        h = Huffman({'a': 49, 'b': 26, 'c': 12, 'd': 5, 'e': 5, 'f': 3})

        expected = {'a': '0', 'b': '11', 'c': '100', 'd': '10111',
                    'e': '1010', 'f': '10110'}
        self._compare_huffman_codes(expected, h.code)

    def test_huffman_code_english_language(self):
        h = Huffman({'a': 0.0812, 'b': 0.0149, 'c': 0.0271, 'd': 0.0432,
                     'e': 0.1202, 'f': 0.0230, 'g': 0.0203, 'h': 0.0592,
                     'i': 0.0731, 'j': 0.0010, 'k': 0.0069, 'l': 0.0398,
                     'm': 0.0261, 'n': 0.0695, 'o': 0.0768, 'p': 0.0182,
                     'q': 0.0011, 'r': 0.0602, 's': 0.0628, 't': 0.0910,
                     'u': 0.0288, 'v': 0.0111, 'w': 0.0209, 'x': 0.0017,
                     'y': 0.0211, 'z': 0.0007})

        correct_code = {'a': '1110', 'b': '101100', 'c': '01000', 'd': '11111',
                        'e': '011', 'f': '00110', 'g': '111100', 'h': '0101',
                        'i': '1100', 'j': '001011001', 'k': '0010111',
                        'l': '10111', 'm': '00111', 'n': '1010', 'o': '1101',
                        'p': '101101', 'q': '001011010', 'r': '1000',
                        's': '1001', 't': '000', 'u': '01001', 'v': '001010',
                        'w': '111101', 'x': '001011011', 'y': '00100',
                        'z': '001011000'}
        self._compare_huffman_codes(correct_code, h.code)


if __name__ == '__main__':
    unittest.main()
