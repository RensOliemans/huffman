import itertools
from functools import reduce
from operator import mul


def expand_probabilities(probabilities: dict, length: int) -> dict:
    c = _compute_product(probabilities.keys(), length)
    probabilities = zip(c, _determine_probabilities(probabilities, c))
    return {x[0]: x[1] for x in probabilities}


def _compute_product(letters, repeat=4):
    return [''.join(l) for l in itertools.product(letters, repeat=repeat)]


def _determine_probabilities(probabilities, strings):
    for s in strings:
        yield reduce(mul, [probabilities[l] for l in s])
