from .__version__ import __title__, __description__, __url__, __version__
from .__version__ import __author__, __author_email__, __license__

from .ex5 import explain

explain()

__all__ = ['__title__', '__description__', '__url__', '__version__',
           '__author__', '__author_email__', '__license__', ]
