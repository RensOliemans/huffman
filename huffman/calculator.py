import math
from queue import PriorityQueue

import loguru

from huffman.tree import Node


class Huffman:
    def __init__(self, distribution, logger=None):
        self.distribution = distribution
        self._logger = logger if logger is not None else loguru.logger

    def print_tree(self) -> None:
        self._logger.info("Huffman tree: {}", self._root.pretty())

    def compare_entropy_code_length(self) -> None:
        """
        Compares the entropy and the expected code length belonging to the
        distribution.
        """
        self._logger.info("Entropy of distribution: {:.3f}", self.entropy)
        self._logger.info("Expected length of huffman coding: {:.3f}",
                          self.expected_code_length)
        self._logger.info("Difference between entropy and "
                          "optimal coding length: {:.3f}",
                          self.expected_code_length - self.entropy)

    @property
    def distribution(self):
        return self._distribution

    @distribution.setter
    def distribution(self, value):
        self._distribution = value
        self._setup()

    def _setup(self):
        """
        Calculates the huffman code and sets the entropy and expected code
        length.
        """
        self._q = None
        self._root = None
        self.code = self._find_huffman_code()
        self.entropy = self._calculate_entropy()
        self.expected_code_length = self._calculate_expected_code_length()

    def _calculate_entropy(self) -> float:
        total = 0
        for _, prob in self._distribution.items():
            if prob == 0:
                total -= 0
            else:
                total -= prob * math.log(prob, 2)
        return total

    def _calculate_expected_code_length(self) -> float:
        expected_value = 0
        for word, probability in self._distribution.items():
            length = len(self.code[word])
            expected_value += length * probability
        return expected_value

    def _find_huffman_code(self) -> dict:
        self._build_huffman_tree()
        return self._get_code(self._root)

    def _build_huffman_tree(self):
        self._build_initial_queue()
        self._complete_queue()

    def _build_initial_queue(self):
        self._q = PriorityQueue()
        for char, prob in self._distribution.items():
            self._q.put(Node(freq=prob, char=char))

    def _complete_queue(self):
        while self._q.qsize() > 1:
            l, r = self._q.get(), self._q.get()
            z = Node(left=l, right=r, freq=l.freq + r.freq)
            self._q.put(z)
        self._root = self._q.get()

    @staticmethod
    def _get_code(node: Node, prefix='', code=None) -> dict:
        """
        Calculates the huffman code starting from some node in a huffman tree.
        """
        if code is None:
            code = {}

        if node.has_left:
            code = {**code, **Huffman._get_code(node.left, prefix+'0', code)}
        if node.has_right:
            code = {**code, **Huffman._get_code(node.right, prefix+'1', code)}
        if node.is_leaf:
            code[node] = prefix
        return code
