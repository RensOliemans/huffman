import loguru

from huffman.helper_functions import expand_probabilities
from huffman.calculator import Huffman

from . import _defaults

logger = loguru.logger


def find_distribution(epsilon: float) -> dict:
    epsilon = 0.1 if epsilon > 0.1 else epsilon
    squared = epsilon**2
    return {'a': 1.0 - squared, 'b': squared}


def debug_huffman(distribution: dict, print_tree: bool = _defaults.PRINT_TREE):
    logger.debug("Trying with distribution {}", distribution)
    h = Huffman(distribution)
    if print_tree:
        h.print_tree()
    h.compare_entropy_code_length()


def main():
    logger.info(_defaults.DISTRIBUTION)

    debug_huffman(_defaults.DISTRIBUTION, print_tree=True)

    distribution = expand_probabilities(_defaults.GIVEN_PROBABILITIES, 12)
    debug_huffman(distribution)


def explain():
    h = Huffman({'a': .999, 'b': .0001})
    print("The exercise is to find a distribution for which the expected "
          "length of the optimal code is close to H(X) + 1. ")

    print("One example is the following distribution: "
          "{'a': 0.999, 'b': 0.0001}.")

    print(f"This distribution has an entropy of {h.entropy:.4f}, and an "
          f"expected optimal code length of {h.expected_code_length:.4f}. "
          f"The difference is {h.expected_code_length - h.entropy:.4f}.")

    print(f"However, for any epsilon e > 0, we need to construct a "
          f"distribution that satisfies:\nE[L] > H(X) + 1 - e\n")

    print("For this, we take the following distribution:\n"
          "{'a': 1.0 - e^2, 'b': e^2},    if epsilon > 0.1,\n"
          "{'a': 0.99,      'b': 0.001},  otherwise.\n")

    print("Check around a bit in the code and play with different "
          "distributions and stuff. To stop this line from being printed, "
          "remove the explain import and method call from "
          "huffman/__init__.py.")


if __name__ == '__main__':
    main()
